import {
  DateRange,
  NumberRange,
  Id
} from "../base";

import { CargoItemTemplate } from "../cargo";
import { MatchQuery } from "../query";
import OutcomingOrder from "./OutcomingOrder";
import { OutcomingOrderState } from "./index";

type OutcomingOrderMatchQuery = MatchQuery<OutcomingOrder, {
  search?: string;
  state?: OutcomingOrderState;
  loadingDate?: DateRange;
  creators?: Id[];
  createdAt?: DateRange;
  carriers?: Id[];
  cargo?: CargoItemTemplate[];
  carrierPrice?: NumberRange;
  isDraft?: boolean;
}>;

export default OutcomingOrderMatchQuery;
