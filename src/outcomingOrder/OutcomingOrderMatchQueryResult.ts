import OutcomingOrder from "./OutcomingOrder";
import { MatchQueryResult } from "../query";

type OutcomingOrderMatchQueryResult = MatchQueryResult<OutcomingOrder>;

export default OutcomingOrderMatchQueryResult;
