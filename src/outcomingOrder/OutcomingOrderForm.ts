import { Contact, ContactEmployee } from "../contact";
import { Journey } from "../journey";
import OutcomingOrderState from "../incomingOrder/IncomingOrderState";

interface OutcomingOrderForm {
  state: OutcomingOrderState;
  indexNumber?: string;

  driverContact: string;
  vehicleInfo: string;
  notes: string;

  isDraft: boolean;

  carrierPrice: number;
  carrierPriceCurrency: string;
  isCarrierPriceWithDph: boolean;
  carrierPaymentDueDays: number;

  carrierContact?: Contact;
  carrierEmployee?: ContactEmployee;

  journey: Journey;
  documents: { id: number }[];
}

export default OutcomingOrderForm;
