import { UserReference } from "../user";
import OutcomingOrderForm from "./OutcomingOrderForm";
import { FileReference } from "../file";

interface OutcomingOrder extends Omit<OutcomingOrderForm, "documents"> {
  id: number;
  companyId: number;

  updatedAt?: Date;
  completedAt?: Date;
  createdAt?: Date;

  createdBy: UserReference;
  editedBy: UserReference;

  documents: FileReference[];
}

export default OutcomingOrder;
