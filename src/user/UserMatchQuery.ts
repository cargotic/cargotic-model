import { MatchQuery } from "../query";
import UserWithStatistics from "./UserWithStatistics";
import UserRole from "./UserRole";

type UserMatchQuery = MatchQuery<UserWithStatistics, {
  search?: string;
  roles?: UserRole[];
}>;

export default UserMatchQuery;
