import UserRole from "./UserRole";

interface User {
  id: number;
  role: UserRole;
  name: string;
  email: string;
  phoneNumber: string;
  avatarUrl?: string;
  lastSeenAt?: Date;
}

export default User;
