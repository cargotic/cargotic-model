enum UserRole {
  ACCOUNTANT = "ACCOUNTANT",
  DISPATCHER = "DISPATCHER",
  DRIVER = "DRIVER",
  MANAGER = "MANAGER",
  OWNER = "OWNER"
}

export default UserRole;
