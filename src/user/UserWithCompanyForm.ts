interface UserWithForm {
  firstName: string;
  lastName: string;
  email: string;
  isSubscriber: boolean;
  companyName: string;
  ic: string;
  dic: string;
  tariffCountry: string;
}

export default UserWithForm;
