import { MatchQueryResult } from "../query";
import UserWithStatistics from "./UserWithStatistics";

type UserMatchQueryResult = MatchQueryResult<UserWithStatistics>;

export default UserMatchQueryResult;
