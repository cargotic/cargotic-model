import UserRole from "./UserRole";

interface UserReference {
  id: number;
  firstName: string;
  lastName: string;
  role: UserRole;
  avatarUrl?: string;
}

export default UserReference;
