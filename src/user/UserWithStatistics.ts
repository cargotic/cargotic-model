import User from "./User";

interface UserWithStatistics extends User {
  weeklyRevenue: number;
  weeklyCommission: number;
  monthlyRevenue: number;
  monthlyCommission: number;
}

export default UserWithStatistics;
