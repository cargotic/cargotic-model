interface MatchQueryResult<T> {
  total: number;
  matches: T[];
}

export default MatchQueryResult;
