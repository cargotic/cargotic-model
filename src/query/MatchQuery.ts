import Ordering from "./Ordering";

interface MatchQuery<T, M> {
  match: M;
  offset?: number;
  limit?: number;
  orderBy?: Ordering<T>;
}

export default MatchQuery;
