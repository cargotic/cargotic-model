enum OrderingDirection {
  ASC = "ASC",
  DESC = "DESC"
}

export default OrderingDirection;
