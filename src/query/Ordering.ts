import { Pair } from "../base";
import OrderingDirection from "./OrderingDirection";

type Ordering<T> = Pair<keyof T, OrderingDirection>[];

export default Ordering;
