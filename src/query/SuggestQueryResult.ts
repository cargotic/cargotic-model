type SuggestQueryResult<T> = T[];

export default SuggestQueryResult;
