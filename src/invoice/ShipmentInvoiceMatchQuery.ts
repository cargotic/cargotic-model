import { MatchQuery } from "../query";
import ShipmentInvoice from "./ShipmentInvoice";
import ShipmentInvoiceType from "./ShipmentInvoiceType";

import { DateRange, Id } from "../base";

type ShipmentInvoiceMatchQuery = MatchQuery<ShipmentInvoice, {
  search?: string;
  creatorId: Id[];
  customer?: Id[];
  duzp?: DateRange;
  type?: ShipmentInvoiceType;
  maturity?: DateRange;
  supplierId?: Id;
  customerId?: Id;
  isPaid?: boolean;
  paymentDate?: DateRange;
  number?: string;
  createdAt?: DateRange;
}>;

export default ShipmentInvoiceMatchQuery;
