import { MatchQueryResult } from "../query";
import ShipmentInvoice from "./ShipmentInvoice";

type ShipmentInvoiceMatchQueryResult = MatchQueryResult<ShipmentInvoice>;

export default ShipmentInvoiceMatchQueryResult;
