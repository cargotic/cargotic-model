import { Id } from "../base";
import ShipmentInvoiceItemsDphType from "./ShipmentInvoiceItemsDphType";

interface ShipmentInvoiceItem {
  id: Id;
  shipmentInvoiceSnapshotId: Id;
  unit: number;
  unitPrice: number;
  dph: number;
  description: string;
  dphType: ShipmentInvoiceItemsDphType;
}

export default ShipmentInvoiceItem;
