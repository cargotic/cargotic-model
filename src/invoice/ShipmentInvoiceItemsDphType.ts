enum ShipmentInvoiceItemsDphType {
  /**
   * Zdanitelné plnění v ČR
   */
  TAXABLE_SUPPLY_CZ = "TAXABLE_SUPPLY_CZ",
  /**
   *  Bez DPH
   */
  WITHOUT_DPH = "WITHOUT_DPH",
  /**
   * Poskytnuté služby do EU
   */
  SERVICES_EU = "SERVICES_EU",
  /**
   * Reverse charge - daň hradí zákazník
   */
  REVERSE_CHARGE = "REVERSE_CHARGE",
  /**
   * Podle zák. 235/2004 sb. se jedná o ZP osv. od povinnosti uplatnit
   * daň dle § 68-69, odberatel je povinen zahrnout tuto službu do hodnoty
   * zboží pro výpočet základu daně dle § 38
   */
  CUSTOMER_INCLUDE = "CUSTOMER_INCLUDE",
  /**
   * Není předmětem daně
   */
  NO_TAX = "NO_TAX",
  /**
   * Osvobozeno od DPH dle §55 zákona o DPH v platném znění
   */
  FREE_FROM_DPH = "FREE_FROM_DPH"
}

export default ShipmentInvoiceItemsDphType;
