enum ShipmentInvoiceType {
  RECEIVED = "RECEIVED",
  ISSUED = "ISSUED"
}

export default ShipmentInvoiceType;
