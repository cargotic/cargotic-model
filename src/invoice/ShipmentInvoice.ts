import { Currency } from "@cargotic-oss/common";
import ShipmentInvoiceType from "./ShipmentInvoiceType";
import ShipmentInvoicePaymentMethod from "./ShipmentInvoicePaymentMethod";
import { Id } from "../base";
import ShipmentInvoiceItem from "./ShipmentInvoiceItem";
import { Company } from "../company";
import { Contact } from "../contact";

interface ShipmentInvoice {
  snapshotId: Id;
  shipmentInvoiceId: Id;
  type: ShipmentInvoiceType;
  dueDate?: Date;
  number?: string;
  paidAt?: Date;
  isPaid?: boolean;
  note?: string;
  isDeleted: boolean;
  paymentMethod: ShipmentInvoicePaymentMethod;
  createdAt: Date;
  expiredAt?: Date;
  vs?: string;
  customer?: Company | Contact;
  supplier?: Company | Contact;
  releaseDate?: Date;
  duzp?: Date;
  price?: number;
  currency?: Currency;
  editorId: Id;
  items?: ShipmentInvoiceItem[];
}

export default ShipmentInvoice;
