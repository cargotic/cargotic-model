import JourneyWaypoint from "./JourneyWaypoint";

interface Journey {
  waypoints: JourneyWaypoint[];
  distance?: number;
  duration?: number;
}

export default Journey;
