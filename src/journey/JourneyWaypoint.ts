import { CargoAction } from "../cargo";
import { Place } from "../place";

interface JourneyWaypoint {
  place: Place;
  contact?: string;
  note?: string;
  arriveAtFrom: Date;
  arriveAtTo?: Date;
  cargo: CargoAction[];
}

export default JourneyWaypoint;
