enum JourneyWaypointType {
  LOAD = "LOAD",
  UNLOAD = "UNLOAD",
  BOTH = "BOTH",
  NONE = "NONE"
}

export default JourneyWaypointType;
