interface SelectedColumns {
    userId: number,
    tableName: string,
    selectedColumns: string[],
}

export default SelectedColumns;
