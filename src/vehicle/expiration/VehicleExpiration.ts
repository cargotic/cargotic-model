import { FileReference } from "../../file";
import VehicleExpirationForm from "./VehicleExpirationForm";

interface VehicleExpiration extends VehicleExpirationForm {
  id: number;
  images: FileReference[];
  createdAt: Date;
}

export default VehicleExpiration;
