import VehicleExpiration from "./VehicleExpiration";

interface VehicleExpirationChange {
  current: VehicleExpiration;
  latest?: VehicleExpiration;
}

export default VehicleExpirationChange;
