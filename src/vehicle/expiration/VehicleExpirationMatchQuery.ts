import { MatchQuery } from "../../query";
import VehicleExpiration from "./VehicleExpiration";
import VehicleExpirationType from "./VehicleExpirationType";

type VehicleExpirationMatchQuery = MatchQuery<VehicleExpiration, {
  type?: VehicleExpirationType;
  search?: string;
  isExpired?: boolean;
}>;

export default VehicleExpirationMatchQuery;
