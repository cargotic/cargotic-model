import { FileReference } from "../../file";
import VehicleExpirationType from "./VehicleExpirationType";

interface VehicleExpirationForm {
  type: VehicleExpirationType;
  expiresAt: Date;
  images: Pick<FileReference, "id">[];
  note?: string;
  name?: string;
}

export default VehicleExpirationForm;
