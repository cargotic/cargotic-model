import { MatchQueryResult } from "../../query";
import VehicleExpiration from "./VehicleExpiration";

type VehicleExpirationMatchQueryResult = MatchQueryResult<VehicleExpiration>;

export default VehicleExpirationMatchQueryResult;
