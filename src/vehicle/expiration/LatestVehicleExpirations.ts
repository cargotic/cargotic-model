import VehicleExpiration from "./VehicleExpiration";
import VehicleExpirationType from "./VehicleExpirationType";

type LatestVehicleExpirations = {
  [K in keyof VehicleExpirationType]?: VehicleExpiration;
};

export default LatestVehicleExpirations;
