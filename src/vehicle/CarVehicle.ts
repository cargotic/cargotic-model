import { Id } from "../base";
import { UserReference } from "../user";

import CarVehicleForm from "./CarVehicleForm";
import TrailerVehicleReference from "./TrailerVehicleReference";

interface CarVehicle extends CarVehicleForm {
  id: Id;
  companyId: Id;
  avatarUrl?: string;
  defaultDriver?: UserReference;
  defaultTrailer?: TrailerVehicleReference;
}

export default CarVehicle;
