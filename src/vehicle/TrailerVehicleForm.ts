import BaseVehicleForm from "./BaseVehicleForm";
import VehicleType from "./VehicleType";

interface TrailerVehicleForm extends BaseVehicleForm {
  type: VehicleType.TRAILER;
}

export default TrailerVehicleForm;
