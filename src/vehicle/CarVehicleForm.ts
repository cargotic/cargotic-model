import BaseVehicleForm from "./BaseVehicleForm";
import VehicleType from "./VehicleType";

interface CarVehicleForm extends BaseVehicleForm {
  type: VehicleType.CAR;
  emissionClass? : string;
  defaultDriver?: {
    id: number;
  };
  defaultTrailer?: {
    id: number;
  };
}

export default CarVehicleForm;
