import { Currency } from "@cargotic-oss/common";

import { TimePeriod } from "../base";

interface VehicleHighlights {
  travels: {
    value: number;
    rateOfChange?: number;
    period: TimePeriod;
  };
  mileage: {
    value: number;
    rateOfChange?: number;
    period: TimePeriod;
    unit: string;
  };
  expenses: {
    value: number;
    rateOfChange?: number;
    period: TimePeriod;
    currency: Currency;
  };
  profit: {
    value: number;
    rateOfChange?: number;
    period: TimePeriod;
    currency: Currency;
  };
}

export default VehicleHighlights;
