interface BaseVehicleForm {
  model: string;
  manufacturer: string;
  vin?: string;
  licensePlate: string;
  manufacturedAt?: Date;
  cargoSpaceDimensions?: {
    width?: number;
    height?: number;
    length?: number;
  };
  cargoSpaceLoad?: number;
}

export default BaseVehicleForm;
