import { Id } from "../base";

import TrailerVehicleForm from "./TrailerVehicleForm";

interface TrailerVehicle extends TrailerVehicleForm {
  id: Id;
  companyId: Id;
  avatarUrl?: string;
}

export default TrailerVehicle;
