import CarVehicle from "./CarVehicle";
import TrailerVehicle from "./TrailerVehicle";

type Vehicle = CarVehicle | TrailerVehicle;

export default Vehicle;
