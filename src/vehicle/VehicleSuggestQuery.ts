interface VehicleSuggestQuery {
  search?: string;
}

export default VehicleSuggestQuery;
