import CarVehicleReference from "./CarVehicleReference";
import TrailerVehicleReference from "./TrailerVehicleReference";

type VehicleReference = CarVehicleReference | TrailerVehicleReference;

export default VehicleReference;
