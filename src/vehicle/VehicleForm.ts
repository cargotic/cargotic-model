import CarVehicleForm from "./CarVehicleForm";
import TrailerVehicleForm from "./TrailerVehicleForm";

type VehicleForm = CarVehicleForm | TrailerVehicleForm;

export default VehicleForm;
