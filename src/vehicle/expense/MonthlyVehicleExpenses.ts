import MonthlyVehicleExpensesForm from "./MonthlyVehicleExpensesForm";

interface MonthlyVehicleExpenses extends MonthlyVehicleExpensesForm {
  total: number;
  rateOfChange?: number;
}

export default MonthlyVehicleExpenses;
