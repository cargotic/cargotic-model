import VehiclExpenseComponent from "./VehicleExpenseComponent";

interface MonthlyVehicleExpensesForm {
  fixed: VehiclExpenseComponent[];
  variable: VehiclExpenseComponent[];
}

export default MonthlyVehicleExpensesForm;
