import { Currency } from "@cargotic-oss/common";

interface VehicleExpenseComponent {
  label: string;
  value: number;
  currency: Currency;
}

export default VehicleExpenseComponent;
