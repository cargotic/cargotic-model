import MonthlyVehicleExpenses from "./MonthlyVehicleExpenses";

type YearlyVehicleExpenses = [
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?,
  MonthlyVehicleExpenses?
];

export default YearlyVehicleExpenses;
