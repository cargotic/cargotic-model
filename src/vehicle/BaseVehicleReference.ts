interface BaseVehicleReference {
  id: number;
  model: string;
  manufacturer: string;
  licensePlate: string;
}

export default BaseVehicleReference;
