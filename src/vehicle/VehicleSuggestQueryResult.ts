import VehicleReference from "./VehicleReference";

type VehicleSuggestQueryResult = VehicleReference[];

export default VehicleSuggestQueryResult;
