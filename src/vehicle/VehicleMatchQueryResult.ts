import { MatchQueryResult } from "../query";
import Vehicle from "./Vehicle";

type VehicleMatchQueryResult = MatchQueryResult<Vehicle>;

export default VehicleMatchQueryResult;
