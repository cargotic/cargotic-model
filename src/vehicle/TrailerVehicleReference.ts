import BaseVehicleReference from "./BaseVehicleReference";
import VehicleType from "./VehicleType";

interface TrailerVehicleReference extends BaseVehicleReference {
  type: VehicleType.TRAILER;
}

export default TrailerVehicleReference;
