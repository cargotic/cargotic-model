import { DateRange, Id } from "../base";
import { MatchQuery } from "../query";
import Vehicle from "./Vehicle";
import VehicleType from "./VehicleType";

type VehicleMatchQuery = MatchQuery<Vehicle, {
  search?: string;
  types?: VehicleType[];
  creators?: Id[];
  createdAt?: DateRange;
}>;

export default VehicleMatchQuery;
