enum VehicleType {
  CAR = "CAR",
  TRAILER = "TRAILER"
}

export default VehicleType;
