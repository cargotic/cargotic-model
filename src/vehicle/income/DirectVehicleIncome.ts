import { Id } from "../../base";
import { UserReference } from "../../user";
import DirectVehicleIncomeForm from "./DirectVehicleIncomeForm";

interface DirectVehicleIncome extends DirectVehicleIncomeForm {
  id: Id;
  companyId: Id;
  vehicleId: Id;
  creator: UserReference;
  createdAt: Date;
}

export default DirectVehicleIncome;
