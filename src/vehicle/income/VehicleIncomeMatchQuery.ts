import { MatchQuery } from "../../query";

import VehicleIncomeMatch from "./VehicleIncomeMatch";
import VehicleIncomeMatchType from "./VehicleIncomeMatchType";

type VehicleIncomeMatchQuery = MatchQuery<VehicleIncomeMatch, {
  search?: string;
  types?: VehicleIncomeMatchType[];
}>;

export default VehicleIncomeMatchQuery;
