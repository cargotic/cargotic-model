import VehicleIncomeBase from "./VehicleIncomeBase";

interface DirectVehicleIncomeForm extends VehicleIncomeBase {
  description: string;
}

export default DirectVehicleIncomeForm;
