import DirectVehicleIncome from "./DirectVehicleIncome";
import ShipmentVehicleIncome from "./ShipmentVehicleIncome";

type VehicleIncome = DirectVehicleIncome | ShipmentVehicleIncome;

export default VehicleIncome;
