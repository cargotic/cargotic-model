import ShipmentVehicleIncome from "./ShipmentVehicleIncome";
import VehicleIncomeMatchType from "./VehicleIncomeMatchType";

interface ShipmentVehicleIncomeMatch {
  type: VehicleIncomeMatchType.SHIPMENT_INCOME;
  income: ShipmentVehicleIncome;
}

export default ShipmentVehicleIncomeMatch;
