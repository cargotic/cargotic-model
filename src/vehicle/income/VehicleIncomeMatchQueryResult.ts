import { MatchQueryResult } from "../../query";

import VehicleIncomeMatch from "./VehicleIncomeMatch";

type VehicleIncomeMatchQueryResult = MatchQueryResult<VehicleIncomeMatch>;

export default VehicleIncomeMatchQueryResult;
