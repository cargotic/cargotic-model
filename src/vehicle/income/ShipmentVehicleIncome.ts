import { Id } from "../../base";
import { UserReference } from "../../user";

import VehicleIncomeBase from "./VehicleIncomeBase";

interface ShipmentVehicleIncome extends VehicleIncomeBase {
  shipmentId: Id;
  shipmentSerialNumber: string;
  creator: UserReference;
  createdAt: Date;
}

export default ShipmentVehicleIncome;
