import DirectVehicleIncomeMatch from "./DirectVehicleIncomeMatch";
import ShipmentVehicleIncomeMatch from "./ShipmentVehicleIncomeMatch";

type VehicleIncomeMatch = DirectVehicleIncomeMatch
  | ShipmentVehicleIncomeMatch;

export default VehicleIncomeMatch;
