enum VehicleIncomeMatchType {
  DIRECT_INCOME = "DIRECT_INCOME",
  SHIPMENT_INCOME = "SHIPMENT_INCOME"
}

export default VehicleIncomeMatchType;
