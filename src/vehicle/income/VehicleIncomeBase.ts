import { Currency } from "@cargotic-oss/common";

interface VehicleIncomeBase {
  value: number;
  currency: Currency;
  accountedAt: Date;
}

export default VehicleIncomeBase;
