import DirectVehicleIncome from "./DirectVehicleIncome";
import VehicleIncomeMatchType from "./VehicleIncomeMatchType";

interface DirectVehicleIncomeMatch {
  type: VehicleIncomeMatchType.DIRECT_INCOME;
  income: DirectVehicleIncome;
}

export default DirectVehicleIncomeMatch;
