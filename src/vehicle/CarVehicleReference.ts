import { UserReference } from "../user";

import BaseVehicleReference from "./BaseVehicleReference";
import TrailerVehicleReference from "./TrailerVehicleReference";
import VehicleType from "./VehicleType";

interface CarVehicleReference extends BaseVehicleReference {
  type: VehicleType.CAR;
  defaultDriver?: UserReference;
  defaultTrailer?: TrailerVehicleReference;
}

export default CarVehicleReference;
