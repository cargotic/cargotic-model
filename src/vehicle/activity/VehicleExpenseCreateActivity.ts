import { UserReference } from "../../user";
import VehicleActivityType from "./VehicleActivityType";

interface VehicleExpenseCreateActivity {
  type: VehicleActivityType.VEHICLE_EXPENSE_CREATE;
  user: UserReference;
  year: number;
  month: number;
  createdAt: Date;
}

export default VehicleExpenseCreateActivity;
