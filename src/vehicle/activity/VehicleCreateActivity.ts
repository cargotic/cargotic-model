import { UserReference } from "../../user";
import VehicleActivityType from "./VehicleActivityType";

interface VehicleCreateActivity {
  type: VehicleActivityType.VEHICLE_CREATE;
  user: UserReference;
  createdAt: Date;
}

export default VehicleCreateActivity;
