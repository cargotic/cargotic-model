import { Commentary } from "../../social";
import VehicleActivityType from "./VehicleActivityType";

interface VehicleCommentaryActivity {
  type: VehicleActivityType.VEHICLE_COMMENTARY;
  commentary: Commentary;
}

export default VehicleCommentaryActivity;
