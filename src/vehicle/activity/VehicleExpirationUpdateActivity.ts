import { UserReference } from "../../user";
import { VehicleExpirationType } from "../expiration";
import VehicleActivityType from "./VehicleActivityType";

interface VehicleExpirationUpdateActivity {
  type: VehicleActivityType.VEHICLE_EXPIRATION_CREATE;
  expirationType: VehicleExpirationType;
  user: UserReference;
  createdAt: Date;
}

export default VehicleExpirationUpdateActivity;
