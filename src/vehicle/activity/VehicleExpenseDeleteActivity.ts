import { UserReference } from "../../user";

import VehicleActivityType from "./VehicleActivityType";

interface VehicleExpenseDeleteActivity {
  type: VehicleActivityType.VEHICLE_EXPENSE_DELETE;
  user: UserReference;
  year: number;
  month: number;
  createdAt: Date;
}

export default VehicleExpenseDeleteActivity;
