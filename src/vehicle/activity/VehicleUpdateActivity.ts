import { UserReference } from "../../user";
import Vehicle from "../Vehicle";
import VehicleActivityType from "./VehicleActivityType";

interface VehicleUpdateActivity {
  type: VehicleActivityType.VEHICLE_UPDATE;
  user: UserReference;
  properties: Partial<Vehicle>;
  createdAt: Date;
}

export default VehicleUpdateActivity;
