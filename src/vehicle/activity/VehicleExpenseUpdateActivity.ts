import { UserReference } from "../../user";
import VehicleActivityType from "./VehicleActivityType";

interface VehicleExpenseUpdateActivity {
  type: VehicleActivityType.VEHICLE_EXPENSE_UPDATE;
  user: UserReference;
  year: number;
  month: number;
  createdAt: Date;
}

export default VehicleExpenseUpdateActivity;
