import VehicleCommentaryActivity from "./VehicleCommentaryActivity";
import VehicleCreateActivity from "./VehicleCreateActivity";
import VehicleExpenseCreateActivity from "./VehicleExpenseCreateActivity";
import VehicleExpenseDeleteActivity from "./VehicleExpenseDeleteActivity";
import VehicleExpenseUpdateActivity from "./VehicleExpenseUpdateActivity";
import VehicleExpirationCreateActivity from "./VehicleExpirationCreateActivity";
import VehicleExpirationDeleteActivity from "./VehicleExpirationDeleteActivity";
import VehicleExpirationUpdateActivity from "./VehicleExpirationUpdateActivity";
import VehicleUpdateActivity from "./VehicleUpdateActivity";

type VehicleActivity =
  VehicleCommentaryActivity
  | VehicleCreateActivity
  | VehicleExpenseCreateActivity
  | VehicleExpenseDeleteActivity
  | VehicleExpenseUpdateActivity
  | VehicleExpirationCreateActivity
  | VehicleExpirationDeleteActivity
  | VehicleExpirationUpdateActivity
  | VehicleUpdateActivity;

export default VehicleActivity;
