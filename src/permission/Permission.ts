const Permission = {
  resource: {
    company: {
      read: "resource.company.read",
      update: "resource.company.update",
      delete: "resource.company.delete",
    },
    bankAccount: {
      read: "resource.bankAccount.read",
      update: "resource.bankAccount.update",
      delete: "resource.bankAccount.delete",
      create: "resource.bankAccount.create"
    },
    role: {
      create: "resource.role.create",
      read: "resource.role.read",
      update: "resource.role.update",
      delete: "resource.role.delete"
    },
    selectedColumns: {
      create: "resource.selectedColumns.create",
      read: "resource.selectedColumns.read",
      update: "resource.selectedColumns.update",
      delete: "resource.selectedColumns.delete"
    },
    terms: {
      user: {
        create: "resource.terms.user.create",
        read: "resource.terms.user.read",
        update: "resource.terms.user.update",
        delete: "resource.terms.user.delete"
      },
      company: {
        create: "resource.terms.company.create",
        read: "resource.terms.company.read",
        update: "resource.terms.company.update",
        delete: "resource.terms.company.delete"
      }
    },
    contact: {
      user: {
        create: "resource.contact.user.create",
        read: "resource.contact.user.read",
        update: "resource.contact.user.update",
        delete: "resource.contact.user.delete"
      },
      company: {
        read: "resource.contact.company.read",
        update: "resource.contact.company.update",
        delete: "resource.contact.company.delete"
      }
    },
    place: {
      create: "resource.place.create",
      read: "resource.place.read",
      update: "resource.place.update",
      delete: "resource.place.delete"
    },
    shipment: {
      user: {
        create: "resource.shipment.user.create",
        read: "resource.shipment.user.read",
        update: "resource.shipment.user.update",
        delete: "resource.shipment.user.delete",
        commentary: {
          create: "resource.shipment.user.commentary.create",
          update: "resource.shipment.user.commentary.update",
          delete: "resource.shipment.user.commentary.delete"
        },
        driveThrough: {
          create: "resource.shipment.user.driveThrough.create",
          delete: "resource.shipment.user.driveThrough.delete"
        },
        extended: {
          create: "resource.shipment.user.extended.create"
        }
      },
      company: {
        read: "resource.shipment.company.read",
        update: "resource.shipment.company.update",
        delete: "resource.shipment.company.delete",
        commentary: {
          create: "resource.shipment.company.commentary.create",
          update: "resource.shipment.company.commentary.update",
          delete: "resource.shipment.company.commentary.delete"
        },
        driveThrough: {
          create: "resource.shipment.company.driveThrough.create",
          delete: "resource.shipment.company.driveThrough.delete"
        }
      },
      summary: {
        read: "resource.shipment.summary.read"
      }
    },
    incomingOrder: {
      user: {
        create: "resource.incomingOrder.user.create",
        read: "resource.incomingOrder.user.read",
        update: "resource.incomingOrder.user.update",
        delete: "resource.incomingOrder.user.delete",
        commentary: {
          create: "resource.incomingOrder.user.commentary.create",
          update: "resource.incomingOrder.user.commentary.update",
          delete: "resource.incomingOrder.user.commentary.delete"
        },
        driveThrough: {
          create: "resource.incomingOrder.user.driveThrough.create",
          delete: "resource.incomingOrder.user.driveThrough.delete"
        },
        extended: {
          create: "resource.incomingOrder.user.extended.create"
        }
      },
      company: {
        read: "resource.incomingOrder.company.read",
        update: "resource.incomingOrder.company.update",
        delete: "resource.incomingOrder.company.delete",
        commentary: {
          create: "resource.incomingOrder.company.commentary.create",
          update: "resource.incomingOrder.company.commentary.update",
          delete: "resource.incomingOrder.company.commentary.delete"
        },
        driveThrough: {
          create: "resource.incomingOrder.company.driveThrough.create",
          delete: "resource.incomingOrder.company.driveThrough.delete"
        }
      },
      summary: {
        read: "resource.incomingOrder.summary.read"
      }
    },
    warehouseOrder: {
      user: {
        create: "resource.warehouseOrder.user.create",
        read: "resource.warehouseOrder.user.read",
        update: "resource.warehouseOrder.user.update",
        delete: "resource.warehouseOrder.user.delete",
      },
      company: {
        read: "resource.warehouseOrder.company.read",
        update: "resource.warehouseOrder.company.update",
        delete: "resource.warehouseOrder.company.delete",
      },
      summary: {
        read: "resource.warehouseOrder.summary.read"
      },
      operation: {
        receive: "resource.warehouseOrder.operation.receive",
        dispatch: "resource.warehouseOrder.operation.dispatch",
      },
    },
    warehouse: {
      create: "resource.warehouse.create",
      read: "resource.warehouse.read",
      update: "resource.warehouse.update",
      delete: "resource.warehouse.delete",
      createWarehouseman: "resource.warehouse.createWarehouseman",
    },
    outcomingOrder: {
      user: {
        create: "resource.outcomingOrder.user.create",
        read: "resource.outcomingOrder.user.read",
        update: "resource.outcomingOrder.user.update",
        delete: "resource.outcomingOrder.user.delete",
        commentary: {
          create: "resource.outcomingOrder.user.commentary.create",
          update: "resource.outcomingOrder.user.commentary.update",
          delete: "resource.outcomingOrder.user.commentary.delete"
        },
        driveThrough: {
          create: "resource.outcomingOrder.user.driveThrough.create",
          delete: "resource.outcomingOrder.user.driveThrough.delete"
        },
        extended: {
          create: "resource.outcomingOrder.user.extended.create"
        }
      },
      company: {
        read: "resource.outcomingOrder.company.read",
        update: "resource.outcomingOrder.company.update",
        delete: "resource.outcomingOrder.company.delete",
        commentary: {
          create: "resource.outcomingOrder.company.commentary.create",
          update: "resource.outcomingOrder.company.commentary.update",
          delete: "resource.outcomingOrder.company.commentary.delete"
        },
        driveThrough: {
          create: "resource.outcomingOrder.company.driveThrough.create",
          delete: "resource.outcomingOrder.company.driveThrough.delete"
        }
      },
      summary: {
        read: "resource.outcomingOrder.summary.read"
      }
    },
    invoice: {
      create: "resource.invoice.create",
      read: "resource.invoice.read",
      export: {
        pohoda: "resource.invoice.export.pohoda"
      },
      pay: "resource.invoice.pay"
    },
    statistics: {
      user: {
        read: "resource.statistics.user.read"
      },
      company: {
        read: "resource.statistics.company.read"
      }
    },
    subscription: {
      read: "resource.subscription.read",
      update: "resource.subscription.update",
      invoice: {
        create: "resource.subscription.invoice.create",
        read: "resource.subscription.invoice.read"
      }
    },
    track: {
      create: "resource.track.create",
      read: "resource.track.read"
    },
    user: {
      create: "resource.user.create",
      read: "resource.user.read",
      update: "resource.user.update",
      delete: "resource.user.delete",
      role: {
        update: "resource.user.role.update",
        permission: {
          driver: "resource.user.role.permission.driver"
        }
      }
    },
    vehicle: {
      read: "resource.vehicle.read",
      update: "resource.vehicle.update",
      delete: "resource.vehicle.delete",
      create: "resource.vehicle.create",

      directIncome: {
        create: "resource.vehicle.directIncome.create",
        update: "resource.vehicle.directIncome.update",
        delete: "resource.vehicle.directIncome.delete"
      },
      expenses: {
        create: "resource.vehicle.expenses.create",
        read: "resource.vehicle.expenses.read",
        update: "resource.vehicle.expenses.update",
        delete: "resource.vehicle.expenses.delete"
      },
      expiration: {
        create: "resource.vehicle.expiration.create",
        read: "resource.vehicle.expiration.read",
        update: "resource.vehicle.expiration.update",
        delete: "resource.vehicle.expiration.delete"
      },
      income: {
        read: "resource.vehicle.income.read"
      }
    },
    documentSequence: {
      read: "resource.documentSequence.read",
      update: "resource.documentSequence.update"
    }
  },
  platform: {
    access: {
      webapp: "platform.access.webapp",
      mobileapp: "platform.access.mobileapp"
    }
  }
};

export default Permission;
