import { Currency } from "@cargotic-oss/common";

interface BankAccountForm {
  currency: Currency;
  iban: string;
  bban: string;
}

export default BankAccountForm;
