import { MatchQuery } from "../query";
import BankAccount from "./BankAccount";

type BankAccountMatchQuery = MatchQuery<BankAccount, {}>;

export default BankAccountMatchQuery;
