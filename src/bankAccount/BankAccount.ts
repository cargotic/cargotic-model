import { Currency } from '@cargotic-oss/common'
interface BankAccount {
  id: number;
  bban: string;
  companyId: number;
  creatorId: number;
  currency: Currency;
  iban: string;
  isDeleted: boolean;
}

export default BankAccount;
