import { MatchQueryResult } from "../query";
import BankAccount from "./BankAccount";

type BankAccountMatchQueryResult = MatchQueryResult<BankAccount>;

export default BankAccountMatchQueryResult;
