interface News {
  imageUrl?: string;
  content: string;
  createdAt: Date;
}

export default News;
