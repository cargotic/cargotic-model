import IncomingOrderMatchQeury from "./IncomingOrderMatchQuery";
import IncomingOrderState from "./IncomingOrderState";

interface IncomingOrderBoardMatchQuery extends IncomingOrderMatchQeury {
  states: IncomingOrderState[];
}

export default IncomingOrderBoardMatchQuery;
