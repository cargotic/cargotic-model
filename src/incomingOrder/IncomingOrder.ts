import { UserReference } from "../user";
import IncomingOrderForm from "./IncomingOrderForm";
import { FileReference } from "../file";

interface IncomingOrder extends Omit<IncomingOrderForm, "documents"> {
  id: number;
  companyId: number;

  updatedAt?: Date;
  completedAt?: Date;
  createdAt?: Date;

  createdBy: UserReference;
  editedBy: UserReference;

  outcomingOrderId?: number;
  shipmentId?: number;

  documents: FileReference[];
}

export default IncomingOrder;
