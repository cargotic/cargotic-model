import IncomingOrder from "./IncomingOrder";
import { MatchQueryResult } from "../query";

type IncomingOrderMatchQueryResult = MatchQueryResult<IncomingOrder>;

export default IncomingOrderMatchQueryResult;
