import {
  DateRange,
  NumberRange,
  Id
} from "../base";

import { CargoItemTemplate } from "../cargo";
import { MatchQuery } from "../query";
import IncomingOrder from "./IncomingOrder";
import IncomingOrderState from "./IncomingOrderState";

type IncomingOrderMatchQuery = MatchQuery<IncomingOrder, {
  search?: string;
  loadingDate?: DateRange;
  unloadingDate?: DateRange;
  creators?: Id[];
  customers?: Id[];
  isDraft?: boolean;
  customerPrice?: NumberRange;
  cargo?: CargoItemTemplate[];
  createdAt?: DateRange;
  outcomingOrderIds?: Id[];
  shipmentIds?: Id[];
}> & {
  state?: IncomingOrderState;
};

export default IncomingOrderMatchQuery;
