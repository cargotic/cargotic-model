import { Contact, ContactEmployee } from "../contact";
import { Journey } from "../journey";
import IncomingOrderState from "./IncomingOrderState";

interface IncomingOrderForm {
  state: IncomingOrderState;
  indexNumber?: string;
  externalReference?: string;

  notes: string;

  isDraft: boolean;

  customerPrice: number;
  customerPriceCurrency: string;
  isCustomerPriceWithDph: boolean;
  customerPaymentDueDays: number;

  customerContact?: Contact;
  customerEmployee?: ContactEmployee;

  journey: Journey;
  documents: { id: number }[];
}

export default IncomingOrderForm;
