import IncomingOrderMatchQueryResult from "./IncomingOrderMatchQueryResult";
import IncomingOrderState from "./IncomingOrderState";

type IncomingOrderBoardMatchQueryResult = {
  [IncomingOrderState.QUEUE]: IncomingOrderMatchQueryResult;
  [IncomingOrderState.IN_PROGRESS]: IncomingOrderMatchQueryResult;
  [IncomingOrderState.READY]: IncomingOrderMatchQueryResult;
  [IncomingOrderState.DONE]: IncomingOrderMatchQueryResult;
};

export default IncomingOrderBoardMatchQueryResult;
