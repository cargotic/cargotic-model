import { Journey } from "../journey";
import { User } from "../user";
import { Vehicle } from "../vehicle";

import TrackingGeoposition from "./TrackingGeoposition";

interface Tracking {
  id: number;
  latestGeoposition: TrackingGeoposition;
  vehicle: Vehicle;
  driver: User;
  journey: Journey;
}

export default Tracking;
