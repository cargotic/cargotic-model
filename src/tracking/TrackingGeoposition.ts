import { Geoposition } from "../place";

interface TrackingGeoposition extends Geoposition {
  sequence: number;
  speed: number;
  trackedAt: Date;
}

export default TrackingGeoposition;
