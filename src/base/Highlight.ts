interface Highlight {
  value: number;
  rateOfChange?: number;
}

export default Highlight;
