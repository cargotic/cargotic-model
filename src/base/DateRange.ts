interface DateRange {
  from: Date;
  to?: Date;
}

export default DateRange;
