type NumberRange = {
  from: number;
  to: number;
};

export default NumberRange;
