interface Session {
  id: number;
  company_id: number;
  email: string;
  permissions: string[];
}

export default Session;
