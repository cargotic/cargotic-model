interface SimplePlace {
  googleId?: string;
  formattedAddress: string;
}

export default SimplePlace;
