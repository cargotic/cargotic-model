interface Geoposition {
  latitude: number;
  longitude: number;
}

export default Geoposition;
