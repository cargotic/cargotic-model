import Geoposition from "./Geoposition";
import PlaceAddress from "./PlaceAddress";

interface Place extends Geoposition {
  id?: number;
  googleId: string;
  name: string;
  address: PlaceAddress;
  alias?: string;
  thumbnailUrl?: string;
}

export default Place;
