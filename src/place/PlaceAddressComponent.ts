interface PlaceAddressComponent {
  types: string[];
  shortName: string;
  longName: string;
}

export default PlaceAddressComponent;
