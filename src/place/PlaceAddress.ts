import PlaceAddressComponent from "./PlaceAddressComponent";

interface PlaceAddress {
  formatted: string;
  components: PlaceAddressComponent[];
}

export default PlaceAddress;
