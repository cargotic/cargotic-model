import Place from "./Place";

type CondensedPlace = Omit<Place, "thumbnailUrl">;

export default CondensedPlace;
