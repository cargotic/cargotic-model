import ShipmentMatchQeury from "./ShipmentMatchQuery";
import ShipmentState from "./ShipmentState";

interface ShipmentBoardMatchQuery extends ShipmentMatchQeury {
  states: ShipmentState[];
}

export default ShipmentBoardMatchQuery;
