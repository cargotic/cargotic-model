import { Commentary } from "../social";
import ShipmentActivityType from "./ShipmentActivityType";

interface ShipmentCommentaryActivity {
  type: ShipmentActivityType.SHIPMENT_COMMENTARY;
  commentary: Commentary;
}

export default ShipmentCommentaryActivity;
