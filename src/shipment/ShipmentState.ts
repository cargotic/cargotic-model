enum IncomingOrderState {
  DONE = "DONE",
  IN_PROGRESS = "IN_PROGRESS",
  QUEUE = "QUEUE",
  READY = "READY"
}

export default IncomingOrderState;
