import ShipmentCommentaryActivity from "./ShipmentCommentaryActivity";
import ShipmentCreateActivity from "./ShipmentCreateActivity";
import ShipmentUpdateActivity from "./ShipmentUpdateActivity";
import WaypointDriveThroughCreateActivity from "./WaypointDriveThroughCreateActivity";
import WaypointDriveThroughUpdateActivity from "./WaypointDriveThroughUpdateActivity";
import WaypointDriveThroughDeleteActivity from "./WaypointDriveThroughDeleteActivity";


type ShipmentActivity =
  ShipmentCommentaryActivity
  | ShipmentCreateActivity
  | ShipmentUpdateActivity
  | WaypointDriveThroughCreateActivity
  | WaypointDriveThroughUpdateActivity
  | WaypointDriveThroughDeleteActivity

export default ShipmentActivity;
