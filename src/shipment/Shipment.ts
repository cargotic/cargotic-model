import { UserReference } from "../user";
import ShipmentForm from "./ShipmentForm";
import { FileReference } from "../file";

interface Shipment extends Omit<ShipmentForm, "documents"> {
  id: number;
  companyId: number;

  updatedAt?: Date;
  completedAt?: Date;
  createdAt?: Date;

  createdBy: UserReference;
  editedBy: UserReference;

  documents: FileReference[];
}

export default Shipment;
