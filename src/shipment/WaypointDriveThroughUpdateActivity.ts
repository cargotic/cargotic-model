import { UserReference } from "../user";
import { JourneyWaypointType } from "../journey";

import ShipmentActivityType from "./ShipmentActivityType";

interface WaypointDriveThroughUpdateActivity {
  address: string,
  user: UserReference,
  createdAt: Date,
  waypointType: JourneyWaypointType,
  type: ShipmentActivityType.WAYPOINT_DRIVE_THROUGH_UPDATE
}

export default WaypointDriveThroughUpdateActivity;