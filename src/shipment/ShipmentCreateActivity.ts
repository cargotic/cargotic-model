import { UserReference } from "../user";
import ShipmentActivityType from "./ShipmentActivityType";

interface ShipmentCreateActivity {
  type: ShipmentActivityType.SHIPMENT_CREATE;
  user: UserReference;
  createdAt: Date;
}

export default ShipmentCreateActivity;
