import Shipment from "./Shipment";
import { MatchQueryResult } from "../query";

type ShipmentMatchQueryResult = MatchQueryResult<Shipment>;

export default ShipmentMatchQueryResult;
