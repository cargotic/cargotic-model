import { UserReference } from "../user";
import { JourneyWaypointType } from "../journey";

import ShipmentActivityType from "./ShipmentActivityType";

interface WaypointDriveThroughDeleteActivity {
  address: string,
  user: UserReference,
  createdAt: Date,
  waypointType: JourneyWaypointType,
  type: ShipmentActivityType.WAYPOINT_DRIVE_THROUGH_DELETE
}

export default WaypointDriveThroughDeleteActivity;