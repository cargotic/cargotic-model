import {
  DateRange,
  NumberRange,
  Id
} from "../base";

import { CargoItemTemplate } from "../cargo";
import { MatchQuery } from "../query";
// import PaymentState from "./ShipmentPaymentState";
import Shipment from "./Shipment";
import {ShipmentState} from "./index";

type ContactMatchQuery = MatchQuery<Shipment, {
  search?: string;
  state?: ShipmentState;
  loadingDate?: DateRange;
  loadingAddresses?: Id[];
  creators?: Id[];
  drivers?: Id[];
  vehicles?: Id[];
  createdAt?: DateRange;
  cargo?: CargoItemTemplate[];
  isDraft?: boolean;
}>;

export default ContactMatchQuery;
