interface ShipmentDuplicate {
  id: number;
  orderSerialNumber: string;
}

export default ShipmentDuplicate;
