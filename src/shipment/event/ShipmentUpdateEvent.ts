import Shipment from "../Shipment";
import ShipmentEventType from "./ShipmentEventType";

interface ShipmentUpdateEvent {
  type: ShipmentEventType.SHIPMENT_UPDATE;
  userId?: number;
  current: Shipment;
  previous: Shipment;
}

export default ShipmentUpdateEvent;
