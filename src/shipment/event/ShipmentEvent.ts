import ShipmentUpdateEvent from "./ShipmentUpdateEvent";

type ShipmentEvent = ShipmentUpdateEvent;

export default ShipmentEvent;
