enum ShipmentEventType {
  SHIPMENT_UPDATE = "SHIPMENT_UPDATE"
}

export default ShipmentEventType;
