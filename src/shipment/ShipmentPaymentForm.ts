interface ShipmentPaymentForm {
  issuedInvoiceNumber?: string;
  issuedInvoiceDueDate?: Date;
  issuedInvoicePaidAt?: Date;
  receivedInvoiceDueDate?: Date;
  receivedInvoiceNumber?: string;
  receivedInvoicePaidAt?: Date;
}

export default ShipmentPaymentForm;
