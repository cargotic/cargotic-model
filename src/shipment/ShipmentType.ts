enum ShipmentType {
  CARRIED = "CARRIED",
  FORWARDED = "FORWARDED"
}

export default ShipmentType;
