import { UserReference } from "../user";
import Shipment from "./Shipment";
import ShipmentActivityType from "./ShipmentActivityType";

interface ShipmentUpdateActivity {
  type: ShipmentActivityType.SHIPMENT_UPDATE;
  user: UserReference;
  properties: Partial<Shipment>;
  createdAt: Date;
}

export default ShipmentUpdateActivity;
