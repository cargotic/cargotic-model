enum ShipmentPaymentState {
  PAID = "PAID",
  UNPAID = "UNPAID",
  OVERDUE = "OVERDUE"
}

export default ShipmentPaymentState;
