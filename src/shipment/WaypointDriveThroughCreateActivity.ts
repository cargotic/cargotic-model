import { UserReference } from "../user";
import { JourneyWaypointType } from "../journey";

import ShipmentActivityType from "./ShipmentActivityType";

interface WaypointDriveThroughCreateActivity {
  address: string,
  user: UserReference,
  createdAt: Date,
  waypointType: JourneyWaypointType,
  type: ShipmentActivityType.WAYPOINT_DRIVE_THROUGH_CREATE
}

export default WaypointDriveThroughCreateActivity;