import ShipmentMatchQueryResult from "./ShipmentMatchQueryResult";
import ShipmentState from "./ShipmentState";

type ShipmentBoardMatchQueryResult = {
  [ShipmentState.QUEUE]: ShipmentMatchQueryResult;
  [ShipmentState.IN_PROGRESS]: ShipmentMatchQueryResult;
  [ShipmentState.READY]: ShipmentMatchQueryResult;
  [ShipmentState.DONE]: ShipmentMatchQueryResult;
}


export default ShipmentBoardMatchQueryResult;
