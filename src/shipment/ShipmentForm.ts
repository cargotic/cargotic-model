import { Journey } from "../journey";
import ShipmentState from "../incomingOrder/IncomingOrderState";
import { UserReference } from "../user";
import { TrailerVehicle, Vehicle } from "../vehicle";

interface ShipmentForm {
  state: ShipmentState;
  indexNumber?: string;

  driver: UserReference;
  vehicle: Vehicle;
  trailer: TrailerVehicle;
  notes: string;

  isDraft: boolean;

  journey: Journey;
  documents: { id: number }[];
}

export default ShipmentForm;
