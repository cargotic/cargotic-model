import { UserReference } from "../user";
import ContactReference from "./ContactReference";

interface ContactDuplicate extends ContactReference {
  createdBy: UserReference;
  isMine: boolean;
  isPrivate: boolean;
  isSharedWithMe: boolean;
}

export default ContactDuplicate;
