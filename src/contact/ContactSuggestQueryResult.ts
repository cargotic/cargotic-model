import ContactReference from "./ContactReference";

type ContactSuggestQueryResult = ContactReference[];

export default ContactSuggestQueryResult;
