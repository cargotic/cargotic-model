import { Vehicle } from "../vehicle";

type ContactVehicle = Omit<Vehicle, "defaultDriver">;

export default ContactVehicle;
