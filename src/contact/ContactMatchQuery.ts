import { MatchQuery } from "../query";
import Contact from "./Contact";
import ContactType from "./ContactType";
import { DateRange, Id } from "../base";

type ContactMatchQuery = MatchQuery<Contact, {
  search?: string;
  types?: ContactType[];
  tags?: Id[];
  createdAt?: DateRange;
  creators?: Id[];
  isPrivate?: boolean;
  sharedWith?: Id[];
  sharedBy?: Id[];
}>;

export default ContactMatchQuery;
