import { VehicleForm } from "../vehicle";

type ContactVehicleForm = Omit<VehicleForm, "defaultDriver">;

export default ContactVehicleForm;
