interface ContactReference {
  id: number;
  name: string;
  ic?: string;
  dic?: string;
}

export default ContactReference;
