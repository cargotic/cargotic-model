interface ContactEmployeeSuggestQuery {
  search?: string;
}

export default ContactEmployeeSuggestQuery;
