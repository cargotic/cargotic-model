interface ContactTag {
  id: number;
  type: string;
  geopoliticalType?: string;
  label: string;
}

export default ContactTag;
