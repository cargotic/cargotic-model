import ContactTag from "./ContactTag";
import ContactType from "./ContactType";

interface ContactForm {
  type: ContactType;
  name: string;
  ic?: string;
  dic?: string;
  email?: string;
  phoneNumber?: string;
  website?: string;
  insuranceExpiresAt?: Date;
  paymentDueDays?: number;
  businessAddress: string;
  billingAddress?: string;
  note?: string;
  tags?: ContactTag[];
  isPrivate: boolean;
}

export default ContactForm;
