import { MatchQueryResult } from "../query";
import ContactVehicle from "./ContactVehicle";

type ContactVehicleMatchQueryResult = MatchQueryResult<ContactVehicle>;

export default ContactVehicleMatchQueryResult;
