interface ContactPrefill {
  name: string;
  ic: string;
  dic: string;
  businessAddress: string;
}

export default ContactPrefill;
