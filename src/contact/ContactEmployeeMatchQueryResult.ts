import { MatchQueryResult } from "../query";
import ContactEmployee from "./ContactEmployee";

type ContactVehicleMatchQueryResult = MatchQueryResult<ContactEmployee>;

export default ContactVehicleMatchQueryResult;
