interface ContactEmployeeForm {
  name?: string;
  email: string;
  phoneNumber?: string;
}

export default ContactEmployeeForm;
