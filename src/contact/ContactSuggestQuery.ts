import ContactType from "./ContactType";

interface ContactSuggestQuery {
  search?: string;
  types?: ContactType[];
}

export default ContactSuggestQuery;
