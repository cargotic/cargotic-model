import ContactEmployeeForm from "./ContactEmployeeForm";

interface ContactEmployee extends ContactEmployeeForm {
  id: number;
}

export default ContactEmployee;
