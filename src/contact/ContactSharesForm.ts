import { UserReference } from "../user";

type ContactSharesForm = Pick<UserReference, "id">[];

export default ContactSharesForm;
