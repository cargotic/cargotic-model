import { UserReference } from "../user";

import ContactForm from "./ContactForm";
import ContactTag from "./ContactTag";

interface Contact extends ContactForm {
  id: number;
  avatarUrl?: string;
  tags: ContactTag[];
  createdBy: UserReference;
  createdAt: Date;
}

export default Contact;
