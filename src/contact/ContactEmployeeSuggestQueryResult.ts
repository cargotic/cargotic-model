import ContactEmployee from "./ContactEmployee";

type ContactEmployeeSuggestQueryResult = ContactEmployee[];

export default ContactEmployeeSuggestQueryResult;
