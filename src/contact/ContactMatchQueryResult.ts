import { MatchQueryResult } from "../query";
import Contact from "./Contact";

type ContactMatchQueryResult = MatchQueryResult<Contact>;

export default ContactMatchQueryResult;
