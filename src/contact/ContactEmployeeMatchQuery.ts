import { MatchQuery } from "../query";
import ContactEmployee from "./ContactEmployee";

type ContactVehicleMatchQuery = MatchQuery<ContactEmployee, { search: string }>;

export default ContactVehicleMatchQuery;
