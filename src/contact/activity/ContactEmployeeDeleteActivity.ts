import { UserReference } from "../../user";
import ContactActicityType from "./ContactActivityType";

interface ContactEmployeeDeleteActivity {
  type: ContactActicityType.CONTACT_EMPLOYEE_DELETE;
  user: UserReference;
  createdAt: Date;
}

export default ContactEmployeeDeleteActivity;
