import { Commentary } from "../../social";
import ContactActivityType from "./ContactActivityType";

interface ContactCommentaryActivity {
  type: ContactActivityType.CONTACT_COMMENTARY;
  commentary: Commentary;
}

export default ContactCommentaryActivity;
