import ContactCommentaryActivity from "./ContactCommentaryActivity";
import ContactCreateActivity from "./ContactCreateActivity";
import ContactEmployeeCreateActivity from "./ContactEmployeeCreateActivity";
import ContactEmployeeDeleteActivity from "./ContactEmployeeDeleteActivity";
import ContactEmployeeUpdateActivity from "./ContactEmployeeUpdateActivity";
import ContactUpdateActivity from "./ContactUpdateActivity";

type ContactActivity =
  ContactCommentaryActivity
  | ContactCreateActivity
  | ContactEmployeeCreateActivity
  | ContactEmployeeDeleteActivity
  | ContactEmployeeUpdateActivity
  | ContactUpdateActivity;

export default ContactActivity;
