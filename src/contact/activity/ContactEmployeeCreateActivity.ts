import { UserReference } from "../../user";
import ContactActivityType from "./ContactActivityType";

interface ContactEmployeeCreateActivity {
  type: ContactActivityType.CONTACT_EMPLOYEE_CREATE;
  user: UserReference;
  createdAt: Date;
}

export default ContactEmployeeCreateActivity;
