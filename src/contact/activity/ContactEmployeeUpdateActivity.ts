import { UserReference } from "../../user";
import ContactActivityType from "./ContactActivityType";

interface ContactEmployeeUpdateActivity {
  type: ContactActivityType.CONTACT_EMPLOYEE_UPDATE;
  user: UserReference;
  createdAt: Date;
}

export default ContactEmployeeUpdateActivity;
