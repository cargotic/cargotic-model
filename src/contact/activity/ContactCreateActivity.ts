import { UserReference } from "../../user";
import ContactActivityType from "./ContactActivityType";

interface ContactCreateActivity {
  type: ContactActivityType.CONTACT_CREATE;
  user: UserReference;
  createdAt: Date;
}

export default ContactCreateActivity;
