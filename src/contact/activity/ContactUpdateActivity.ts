import { UserReference } from "../../user";
import Contact from "../Contact";
import ContactActivityType from "./ContactActivityType";

interface ContactUpdateActivity {
  type: ContactActivityType.CONTACT_CREATE;
  user: UserReference;
  properties: Partial<Contact>;
  createdAt: Date;
}

export default ContactUpdateActivity;
