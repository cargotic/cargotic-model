import { MatchQuery } from "../query";
import ContactVehicle from "./ContactVehicle";

type ContactVehicleMatchQuery = MatchQuery<ContactVehicle, { search: string }>;

export default ContactVehicleMatchQuery;
