enum ContactType {
  BOTH = "BOTH",
  CARRIER = "CARRIER",
  CUSTOMER = "CUSTOMER"
}

export default ContactType;
