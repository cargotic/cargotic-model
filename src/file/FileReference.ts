interface FileReference {
  id: number;
  name: string;
  url: string;
}

export default FileReference;
