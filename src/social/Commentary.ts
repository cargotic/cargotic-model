import { UserReference } from "../user";
import CommentaryForm from "./CommentaryForm";

interface Commentary extends CommentaryForm {
  id: number;
  author: UserReference;
  createdAt: Date;
  editedAt?: Date;
}

export default Commentary;
