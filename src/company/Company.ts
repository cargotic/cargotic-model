import { SimplePlace } from "../place";

interface Company {
  name: string;
  ic: string;
  dic?: string;
  website?: string;
  responsiblePerson?: string;
  email?: string;
  phoneNumber?: string;
  avatar?: string;
  headquarters?: SimplePlace;
  conditions?: string;
  bankAccountIban?: string;
  bankAccountBban?: string;
}

export default Company;
