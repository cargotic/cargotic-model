enum CargoActionType {
  LOAD = "LOAD",
  UNLOAD = "UNLOAD"
}

export default CargoActionType;
