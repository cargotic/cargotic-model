enum CargoItemTemplate {
  EUR_PALLET = "EUR_PALLET",
  US_PALLET = "US_PALLET",
  HALF_EUR_PALLET = "HALF_EUR_PALLET",
  LDM = "LDM",
  LKW = "LKW",
  TANDEM = "TANDEM"
}

export default CargoItemTemplate;
