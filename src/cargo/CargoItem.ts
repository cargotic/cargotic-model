import { LengthUnit } from "@cargotic-oss/common";

import CargoItemTemplate from "./CargoItemTemplate";

interface CargoItem {
  template?: CargoItemTemplate;
  lengthUnit: LengthUnit;
  quantity: number;
  description?: string;
  width?: number;
  height?: number;
  length?: number;
  volume?: number;
  weight?: number;
  totalVolume?: number;
  totalWeight?: number;
}

export default CargoItem;
