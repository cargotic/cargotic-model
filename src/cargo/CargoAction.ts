import CargoActionType from "./CargoActionType";
import CargoItem from "./CargoItem";

interface CargoAction extends CargoItem {
  id: number;
  itemId: number;
  type: CargoActionType;
}

export default CargoAction;
